// <!-- Name Savita Sharma  -->
//import dependencies
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
//concept of programming js
const { check, validationResult, header } = require('express-validator');
var myApp = express();
myApp.use(express.urlencoded({ extended: false }));
//parsing
myApp.use(express.json());
// set path

myApp.set('views', path.join(__dirname, 'views'));
//use public folder for CSS 
myApp.use(express.static(__dirname + '/public'));
myApp.set('view engine', 'ejs');
//MongoDB
// Takes two arguments path - Includes type of DB, ip with port and name of database
// If awesomestore was not created this would create it through code!!!
mongoose.connect('mongodb://localhost:27017/assignment3',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

//Models - add model and then update post handler


// set up the model for the order
const Order = mongoose.model('Order',{
    name: String,
    phonenumber: String,
    province: String,
    address: String,
    city: String,
    notebook: String,
    pencil : String,
    sharpner : String,
    penciltotal : Number,
    pencilprice : Number,
    sharpnerprice: Number,
    notebookprice : Number,
    notebooktotal:Number,
    sharpnertotal:Number,
    subTotal: Number,
    tax: Number,
    total: Number
    } );
//mainpage
//var phoneNumberRegex = /\(?^[5-9][0-9]{9}$/;
 var phoneNumberRegex = /\(?\d{3}\)?-? *\d{3}-? *-?\d{4}/;
myApp.get('/', function (req, res) {
    1
    res.render('form'); //there is no requirement to add ejs 
});


myApp.post('/', [
    check('flname', 'Name is required!').notEmpty(),
   check('address', 'Address is required').notEmpty(),
    check('city', 'City is required').notEmpty(),
    check('province', 'Province is required').notEmpty(),
    // check('phonenumber', '').custom(phonevalidation),//passing  the handler . 
    check('pencilqty', '').custom(validateNumbers), // input numeric value only
    check('notebookqty', '').custom(validateNumbers),
    check('sharpnerqty', '').custom(validateNumbers)

], function (req, res) {
    const errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty()) {
        res.render('form', {
            errors: errors.array()

        });
    }
    else {
        var name = req.body.flname;
       var phonenumber = req.body.phonenumber;
        var province = req.body.province;
        var address = req.body.address;
        var city = req.body.city;
        var notebook = req.body.notebookqty;
        var pencil= req.body.pencilqty;
        var sharpner = req.body.sharpnerqty;
        
        console.log('notebook' + notebook);

       //intialise variables
        var notebookprice = 1.80;
        var pencilprice = 1.00;
        var sharpnerprice = 6.00;
        var taxAmount = 0.13;

        console.log('notebookprice ' + notebookprice +'pencilprice' +pencilprice +'sharpnerprice'+sharpnerprice);

        var notebooktotal = notebook* notebookprice;
        var penciltotal= pencil * pencilprice;
        var sharpnertotal= sharpner* sharpnerprice;
    
        console.log('total value ' + notebooktotal+""+penciltotal+""+sharpnertotal);
    
        var subTotal = notebooktotal + penciltotal + sharpnertotal;

    
        if (subTotal < 10.0) {
        
             
            throw new Error('Minimum purchase should be $10 or more.');
            
        }
        
        // Calculations
        var tax = subTotal * taxAmount;
        var total = subTotal + tax;

        var pageData = {
            name: name,
            phonenumber: phonenumber,
            province: province,
            address: address,
            city: city,
            notebook: notebook,
            pencil : pencil,
            sharpner : sharpner,
            penciltotal : penciltotal,
            pencilprice : pencilprice,
            sharpnerprice: sharpnerprice,
            notebookprice : notebookprice,
            notebooktotal:notebooktotal,
            sharpnertotal:sharpnertotal,
            subTotal: subTotal,
            tax: tax,
            total: total
        }
        var myNewOrder = new Order(
            pageData
        );
        myNewOrder.save().then(() => console.log('New order saved'));

        res.render('form', pageData);
      
    }
});

//All orders page
//fetch all. If there is an error it will put it in the err variable otherwise the orders
//will be returned in the orders variable.
myApp.get('/allorders', function(req, res){
    Order.find({}).exec(function(err, orders){
        console.log(err);
        res.render('allorders', {orders:orders});//key value pair
    });
});
//Function to check a string using regex
function checkRegex(userInput, regex) {
    if (regex.test(userInput)) {
        return true;
    }
    return false;
}

// Custom validation functions return true 
// Custom phone validation
function phonevalidation(value) {
    if (!checkRegex(value, phoneNumberRegex)) {
        throw Error('Phone should be in the format (xxx)-xxx-xxxx');
    }
    return true;
}
//  phone validation


//validation 
function validateNumbers(value, errorMessage) {

    var input = value;

    if (input != '') {
        if (!/^[0-9]+$/.test(input)) {
            throw new Error('quantity must be in numeric form.');
            
        }
        return true
    }

}


// start the server and listen at a port
myApp.listen(1254);

//everything was ok
console.log('Everything executed fine.. website at port 8080....');


