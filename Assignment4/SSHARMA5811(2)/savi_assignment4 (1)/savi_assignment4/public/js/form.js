var postRegex = /^[ABCDEFGHIJKLMNPOQRSTUVWXYZ][0-9][ABCDEFGHIJKLMNOPQRSTUVWXYZ] ?[0-9][ABCDEFGHIJKLMNOPQRSTUVWXYZ][0-9]$/;
var emptyRegex = /^$/;
var anythingRegex = /^.+$/; 
var phoneNumberRegex = /\(?\d{3}\)?-? *\d{3}-? *-?\d{4}/;
var emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// Variables 

// firstly hide the invoice form 
function hideInvoice(){
    document.getElementById('formData').style.visibility = 'hidden';
    
}

var taxAmount = 13;
var formErrors = '';


//general purpose validation method using regex
function validateInputRegex(userInput, simpleRegex, errorMessage) {
    if (!simpleRegex.test(userInput)) {
        formErrors += `${errorMessage} <br>`; // errorMessage + '<br>';
    }
}

// Method to format postal code 
function formatsPostcode(val, e) {
    if (val.value.length < 7) {
        val.value = val.value.replace(/\W/gi, '').replace(/(?=.{2}$)/, ' ');
       return true;
    } else {
        return false;
    }
}

// Method to format phone number 
function formatPhoneNumber(val, e) {
    if (val.value.length < 16) {
        const input =  val.value.replace(/\D/g,'').substring(0,10); // First ten digits of input only
        const areaCode = input.substring(0,3);
        const middle = input.substring(3,7);
        const lastnumber = input.substring(6,10);
    
        if(input.length > 6){val.value = `(${areaCode})-${middle}-${lastnumber}`;}
        else if(input.length > 3){val.value = `(${areaCode}) ${middle}`;}
        else if(input.length > 0){val.value = `(${areaCode}`;}
    

        return true;
    } else {
        return false;
    }
  
}

//Method to validate input  number only
function validate(value, errorMessage) {

    var input = value;

    if (input != '') {
        if (!/^[0-9]+$/.test(input)) {
            formErrors += `${errorMessage} <br>`; // errorMessage + '<br>';
            return
        }
    }

}


//Method to calculate Total receipt 
function calculatetotalitems() {

    // Get quantity for items
    var qty_notebook = document.getElementById('notebookqty').value;
    var qty_pencil = document.getElementById('pencilqty').value;
    var qty_sharpner = document.getElementById('sharpnerqty').value;



    // Check if one item is added
    if (qty_notebook == '' || qty_pencil == '' || qty_sharpner == '') {
        alert("Please add more item")
        return
    }

    // Validate quantity in number
    validateNumbers(qty_notebook, ' quantity should be in number');
    validateNumbers(qty_pencil, ' quantity should be in number');
    validateNumbers(qty_sharpner, 'quantity should be in number');

//checking there is errors for item numbers and display them-------
    if (formErrors) {
        document.getElementById('errors').innerHTML = formErrors;
    }
    else {
        // priceperunit

        var floatValue_pencil = parseFloat(pencil_price).toFixed(2);
        var floatValue_sharpner = parseFloat(sharpner_price).toFixed(2);
        var floatValue_notebook= parseFloat(notebook_price).toFixed(2);
        var floatValueTax = parseFloat(taxAmount).toFixed(2);

        document.getElementById('notebookprice').innerHTML = '$' + floatValue_notebook;
        document.getElementById('pencilprice').innerHTML = '$' + floatValue_pencil;
        document.getElementById('sharpnerprice').innerHTML = '$' + floatValue_sharpner;
         document.getElementById('taxPrice').innerHTML = '$' + floatValueTax;

        // Put quantity for items
        document.getElementById('notebookqty').innerHTML =qty_notebook;
        document.getElementById('pencilqty').innerHTML = qty_pencil;
        document.getElementById('sharpnerqty').innerHTML = qty_sharpner;


        // Calculate items price by multiplying unit price of item
        let totalvalue_notebook = qty_notebook * notebook_price;
        let totalvalue_sharpner = qty_sharpner * sharpner_price;
        let totalvalue_pencil = qty_pencil * pencil_price;

        // Put totals value for each items
        document.getElementById('notebooktotal').innerHTML =  totalvalue_notebook.toFixed(2);
        document.getElementById('sharpnertotal').innerHTML = totalvalue_sharpner.toFixed(2);
        document.getElementById('penciltotal').innerHTML =  totalvalue_pencil.toFixed(2);
         // calculation
        let subTotalValue = totalvalue_pencil + totalvalue_sharpner + totalvalue_pencil;
        document.getElementById('subTotal').innerHTML = '$' + subTotalValue.toFixed(2);

        calculatetax(subTotalValue)
    }

}
// calculate tax and total value
function calculatetax(totalPrice) {

    var taxAmt = (totalPrice * taxAmount) / 100;
    document.getElementById('taxPrice').innerHTML = '$' + taxAmt.toFixed(2);
    var netPrice = totalPrice + taxAmt;
    document.getElementById('TotalValue').innerHTML = '$' + netPrice.toFixed(2);
    return netPrice;
}
